const users = {
    "John": {
        age: 24,
        designation: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        designation: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        designation: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        designation: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        designation: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}



/*
NOTE: Do not change the name of this file
*/

// Q1 Find all users who are interested in playing video games.
const usersInterestedInGames = Object.keys(users).filter((userName) => {
    if (users[userName].interests[0].includes("Video Games")) {
        return userName;
    }
});
console.log("Users who are interested in playing video games:-");
console.log(usersInterestedInGames);



// Q2 Find all users staying in Germany.
const usersStayingInGermany = Object.keys(users).filter((userName) => {
    if (users[userName].nationality === "Germany") {
        return userName;
    }
})
console.log("\nUsers who are  staying in Germany:-")
console.log(usersStayingInGermany);



// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
const sortedUsers = Object.keys(users).sort((user1, user2) => {
    // Assuming seniority level
    const seniorityOrder = {
        "Senior Golang Developer": 3,
        "Senior Javascript Developer": 3,
        "Python Developer": 2,
        "Intern - Golang": 1,
        "Intern - Javascript": 1,
    };

    // Compare based on seniority level
    const seniorityComparison = seniorityOrder[users[user1].designation] - seniorityOrder[users[user2].designation];

    // If seniority is the same, compare based on age
    const ageComparison = users[user1].age - users[user2].age;

    // Combine the comparisons
    return seniorityComparison || ageComparison;
});
console.log("\nSorting users based on seniority level and age:-");
console.log(sortedUsers);



// Q4 Find all users with masters Degree.
const usersHavingMasters = Object.keys(users).filter((userName) => {
    if (users[userName].qualification.includes("Masters")) {
        return userName;
    }
});
console.log("\nUsers who have Masters:-");
console.log(usersHavingMasters);



// Q5 Group users based on their Programming language mentioned in their designation.
groupedUsers = Object.keys(users).reduce((userGroup, userName) => {
    if (users[userName].designation.includes("Golang")) {
        userGroup["Golang"].push(userName);
    } else if (users[userName].designation.includes("Javascript")) {
        userGroup["JavaScript"].push(userName);
    } else {
        userGroup["Python"].push(userName);
    }
    return userGroup;
}, { "Golang": [], "JavaScript": [], "Python": [] })
console.log("\nDesignated user groups:-");
console.log(groupedUsers);
